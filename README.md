# Práctica | Mi primera API con express + ORM

En en esta práctica vamos a subir toda la estructura del CRUD del video club, que estamos desarrollando, se debe subir en una rama distinta a la rama master. Aparte de esto se uso Railway
para crear una instancia de la base de datos.

## Requisitos

- Node
- NPM
- Express

## Instalación 

- Clonar el repositorio:

  git clone git@gitlab.com:hi7114906/mi-primera-api-con-express-orm.git 

- Después se instala en terminal:
 
  npm install


## Back4app

https://videoclub2-ohiknqm5.b4a.run/


## Uso 

- Ejecutar en la terminal:
  npm run dev (NOTA: Podemos hacer pruebas con apoyo en postman)

## Autor

Zaid Joel González Mendoza 353254

## Docente

Ing. Luis Antonio Ramírez Martínez


